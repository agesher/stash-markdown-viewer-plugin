define('plugin/stash-markdown-button',
    ['exports', 'jquery', 'underscore', 'aui', 'eve', 'model/page-state', 'util/navbuilder'],
    function (exports, $, _, AJS, events, pageState, navBuilder) {

        // TODO Shouldn't have to do this
        var revision = navBuilder.parse(location.href).getQueryParamValue('at') || 'master';
        exports.addButton = function (parentSelector) {
            // TODO Check file name

            var $button = $('<a class="aui-button" title="View the file as Markdown">Markdown View</a>');

            var appendButton = function () {
                var filePath = pageState.getFilePath().toString();
                var fileExt = _.last(_.last(filePath.split('/')).split('.'));
                if (!(fileExt === 'md' || fileExt === 'markdown')) {
                    return;
                }
                $button.clone().appendTo($(parentSelector)).click(function () {
                    $(this).attr('aria-pressed', 'true');
                    var errorHandler = function (xhr, status, thrown) {
                        /* happy path fo' lyfe */
                        alert("error: " + status + " " + thrown);
                    };

                    var url = AJS.format("/rest/readme/1.0/projects/{0}/repos/{1}/markup/{2}?at={3}",
                        pageState.getProject().getKey(),
                        pageState.getRepository().getSlug(),
                        filePath,
                        revision
                    );
                    $.ajax(AJS.contextPath() + url, {
                        type: "GET",
                        success: function (data) {
                            $(".content-view").html($('<div style="padding: 5px; white-space: normal"></div>').html(data))
                        },
                        error: errorHandler
                    });
                });
            };

            // TODO no client-side web-items.. re-add the button whenever the source view is updated
            events.on("stash.feature.sourceview.dataLoaded", appendButton);
            events.on("stash.feature.diffview.dataLoaded", appendButton);

            events.on('stash.page.source.revisionRefChanged', function(e) {
                revision = e.getLatestChangeset();
            });
        }

    }
);

AJS.$(document).ready(function () {
    require('plugin/stash-markdown-button').addButton(".source-commands .aui-toolbar2-primary, .source-commands .primary");
});